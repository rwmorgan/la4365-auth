<?php

header('Access-Control-Allow-Origin: *');  

require __DIR__ . '/vendor/autoload.php';


try {

$jwt = new \Firebase\JWT\JWT();

$privateKey = <<<EOD
-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsQJruSa8gHxsv
cr402FCXgqQ1CciYQi95G9FzwC1OrFCNflc3ZK+JKU2VTJt98p2a6klWkfPB0upx
T1MNZgbn8Sdyx7E3yuXLCXvDAdYMIzI4Cundx5BXAFmKu1V2PhYzMUcQ97gv2ta5
bS72rbuYQeCfYCBbtHtfDP5aiDylW+EPpJZXImVA88Ky86hkRReSxVr3jpOnu59s
zsEbM3B0+urfB1g1nGjGMZ6XBHjQYzBJbq+SWOwZRJF16Spw8SLvo3H1rlfgSmfq
qFHvxWPFNpRoQA4yqiorXTEZzdQz2Kca5ouQbrHIrNf9SuRyxoRukwKvp0ZYmH+9
sOBUniGjAgMBAAECggEAVg/RWM0n2nywMHy729tYKNr1ACrrNmebIH9tns+d03j0
pj1BhkOgEH+6a6Xi6oKVjBkSm1ZA285tpD9l14PqX2h1tTtgp+eP+Gm8lCoOr/HF
3YpWXqCe1VkvY8lFYdaTcPBl+WxLcl18exi6JjWXTPNdL0Pv5uG94jezvbfj1RnI
wAMID2IbfiM4LsytCNG/zO/V4JyN8GBRFIRrpT3YwcBhvXw2/m6g7HRtuwSny9kC
cvYQMVQMVOHptHtcEXguDhNra1S75atCX0KSQD2rOWCBxF1DObbg/po0EpuukhKN
TcNU+q+78oahKRhlG7gbuHb1E8V9UQjYYVZKfqyfwQKBgQDcAww0+YA/hhg7M0g+
ZQgO2aPtiuoGiG1o9CEzvRW3JQmUeAmaEwA7N1cfvn7qr7YAnG8+xJl1uq85SuC9
cusOuq3ZfYNAsaGOfDmObT7Sp4HVYtmJl/ZeKNwEkSekl84+cNLswgqleh8uwHb+
S0PSV8R2byrNU/pUpnvdMFu6JQKBgQDIbaMDNyaYz/IPFQ20wkQIy8WmlneHlGeo
VFu9s9rQxSUJHMPqVZp+h957OPLc/nUok7mtalRW9tVnAQZrhBa4RzYRkV+Cjfxp
BFD650mewiwfHa0CpGFnJKEtLyXlMpRjyIgPJ2tQZs0NwDlsyIZz6gDKlreB6Ty1
/OK8DP/OJwKBgHYQiJ9w+LqeGijCt1UMYIufjZGuxzFkStpUj/C02o9ip8ZtuDFf
WvNsGivlzTPhwRiI2kXjuLptyRREs1/W6H8J/LtG2IWCDUFqNnTQ+TKQQ2MgXb6x
trsG0DL/NjfkYwAtCXOWGaJ9a3hqPjDucOHSyZIIy/5Nv3tnr/aduculAoGAC1ry
zl7Uh/nYC/S0ZxZOhnUNCKgjs4G78bR90hybfBJ/S6nei6s36o1GFJMgZ1bfSmI8
Nc/9YUPwyToso/DC3MPkE1C6KYpjz4tYDy8FY8I6f7TNtYWcoxxXKVkve3HHKHQX
CUzBbre0TGAkoL4np++NBe1ODkUplGH0cpUF9ScCgYBGPMdQBSLSrDpUvRQa4Zj+
svMPHxd456UOZ5HBVItViFlxzGvCdiVj7ufpKCPkIiIKFhrM+ZQcUfKdgosk8+ZT
DQfT2GuQd+/iUQS1A+CpGpXD9ApByxc7VeLA3pK4lvTIpqx7XDMDPjiUovrVko4K
bFCBSffw/oVVIS459YDFiw==
-----END PRIVATE KEY-----
EOD;

$publicKey = <<<EOD
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArECa7kmvIB8bL3K+NNhQ
l4KkNQnImEIveRvRc8AtTqxQjX5XN2SviSlNlUybffKdmupJVpHzwdLqcU9TDWYG
5/EncsexN8rlywl7wwHWDCMyOArp3ceQVwBZirtVdj4WMzFHEPe4L9rWuW0u9q27
mEHgn2AgW7R7Xwz+Wog8pVvhD6SWVyJlQPPCsvOoZEUXksVa946Tp7ufbM7BGzNw
dPrq3wdYNZxoxjGelwR40GMwSW6vkljsGUSRdekqcPEi76Nx9a5X4Epn6qhR78Vj
xTaUaEAOMqoqK10xGc3UM9inGuaLkG6xyKzX/UrkcsaEbpMCr6dGWJh/vbDgVJ4h
owIDAQAB
-----END PUBLIC KEY-----
EOD;

$payload = array(
    "iss" => "http://example.org",
    "sub" => "C7A2E5B9-88DF-E311-B8E5-6C3BE5A8B200",
    "iat" => time(),
    "exp" => time() + (60 * 60 * 2)
);

/**
 * IMPORTANT:
 * You must specify supported algorithms for your application. See
 * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
 * for a list of spec-compliant algorithms.
 */
$token = $jwt::encode($payload, $privateKey, 'RS256');

echo $token;

} catch (\Lindelius\JWT\Exception\Exception $exception) {
    // This catches any exception thrown by the JWT library

    echo $exception;
}



?>